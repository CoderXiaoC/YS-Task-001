CREATE TABLE student (
  id          BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name        VARCHAR(255) NOT NULL,
  qq          VARCHAR(32),
  description TEXT,
  create_at   DATETIME NOT NULL,
  update_at   DATETIME
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;