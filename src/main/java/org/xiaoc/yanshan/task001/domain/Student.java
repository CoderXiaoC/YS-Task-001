package org.xiaoc.yanshan.task001.domain;

import java.util.Date;

public class Student {

  private long id;

  private String name;

  private String qq;

  private String description;

  private Date createdAt;

  private Date updatedAt;

  public long getId() {

    return id;
  }

  public void setId(long id) {

    this.id = id;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public String getQq() {

    return qq;
  }

  public void setQq(String qq) {

    this.qq = qq;
  }

  public String getDescription() {

    return description;
  }

  public void setDescription(String description) {

    this.description = description;
  }

  public Date getCreatedAt() {

    return createdAt;
  }

  public void setCreatedAt(Date createdAt) {

    this.createdAt = createdAt;
  }

  public Date getUpdatedAt() {

    return updatedAt;
  }

  public void setUpdatedAt(Date updatedAt) {

    this.updatedAt = updatedAt;
  }
}
